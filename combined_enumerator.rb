# The aim of this class is to take a number of ordered enumerators and then
# emit their values in ascending order.
#
# Some assumptions:
#   * The enumerators passed in emit their values in ascending order.
#     * Enforced with exceptions.
#   * The enumerators emit values which are Comparable[1] with each other.
#   * The enumerators can be finite *or* infinite.
#
# Logic:
#   The main idea is that we always look at the first value of each enumerator
#   and yield the smallest one (peek at the first element without consuming it).
#   This ensures that we only consume one element at a time (the smallest).
#   Before yielding that smallest value we need to make sure that the element
#   is consumed from the enumerator it belongs
#
#   At each iteration a buffer will hold all the values of the first element
#   of each enumerator allowing us to sort that buffer and pick the smallest.
#
#   We will continue yielding values as long as any of the enumerators is not
#   empty.
#
#   Raises:
#     UnorderedEnumerator: If any enumerator is not emitting values in 
#         ascending order.
class CombinedOrderedEnumerator < Enumerator
  class UnorderedEnumerator < RuntimeError; 
    attr_accessor :enumerator
  end

  # Initializes the class.
  #
  # Arguments:
  #     enumerators: A list of enumerators.
  def initialize(*enumerators)
    super() do |yielder|
      while true
        # The buffer contains a tuple (val, enum) where "val" is
        # the first element of the enumerator "enum".
        buffer = []

        enumerators.each do |enumerator|
          begin
            buffer << [enumerator.peek, enumerator]
          rescue StopIteration # Ignore empty enumerators.
          end
        end

        # Stop when all buffers are empty.
        break if buffer.size <= 0

        # Sort buffer by value.
        sorted_buffer = buffer.sort_by { |value, _| value }
        value, enumerator = sorted_buffer.first

        # Verify the enumerator is is ascending order.
        # This can be done by checking that at any point each two adjacent
        # elements are also in ascending order.
        if enumerator.take(2) != enumerator.take(2).sort
          exception = UnorderedEnumerator.new
          exception.enumerator = enumerator
          raise exception
        end

        enumerator.next # Consume the value.
        yielder << value
      end
    end
  end
end
