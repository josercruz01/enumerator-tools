# README #

### Project ###

* Summary

Tools for performing advanced operations on Ruby Enumerators.

### How do I get set up? ###

* Summary of set up

```
#!bash

git clone https://bitbucket.org/josercruz01/enumerator-tools
cd enumerator-tools
```

* How to run tests


```
#!bash
./run_tests.sh
```